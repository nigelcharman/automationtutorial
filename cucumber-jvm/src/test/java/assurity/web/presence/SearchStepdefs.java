package assurity.web.presence;

import static org.junit.Assert.*;

import org.openqa.selenium.WebDriver;

import cucumber.annotation.After;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;
import driver.core.DriverFactory;
import driver.google.search.GoogleResultsPage;
import driver.google.search.GoogleSearchPage;

public class SearchStepdefs {
    private WebDriver driver = new DriverFactory().getDriver();
    private GoogleResultsPage resultsPage;

    @After
    public void closeBrowser() {
        driver.quit();
    }

    @Given("^I am using ([^\"]*)$")
    public void I_am_using(String engine) {
    }

    @When("^I search for '([^\"]*)'$")
    public void I_search_for(String keyword) {
        GoogleSearchPage searchPage = new GoogleSearchPage(driver);
        resultsPage = searchPage.searchFor(keyword);
    }

    @Then("^the top result should be '([^\"]*)'$")
    public void the_search_results_should_be(String expectedResults) {
        assertEquals(expectedResults, resultsPage.getResult());
    }

    @Then("^the results include the text '([^\"]*)'$")
    public void the_results_include_the_text(String expectedText) {
        assertTrue(resultsPage.resultsContain(expectedText));
    }
}
