package driver.google.search;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A WebDriver Page Object corresponding to the Google Results Page.
 */
public class GoogleResultsPage {

    @FindBy(className = "s")
    private List<WebElement> resultsText;

    @FindBy(className = "l")
    private WebElement firstResultLink;

    @FindBy(className = "r")
    private WebElement resultLink;

    @FindBy(id = "foot")
    private WebElement footer;

    private final WebDriver driver;

    public GoogleResultsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        waitForFooter();
    }

    public boolean resultsContain(String text) {
        for (WebElement result : resultsText) {
            if (result.getText().contains(text)) {
                return true;
            }
        }
        return false;
    }

    public String getTopResultTitle() {
        return firstResultLink.getText();
    }

    public String getResult() {
        return resultLink.getText();
    }

    private void waitForFooter() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(footer));
    }
}