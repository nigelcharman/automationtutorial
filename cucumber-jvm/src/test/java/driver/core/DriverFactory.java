package driver.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {
    
	public WebDriver getDriver() {
        return new FirefoxDriver();
    }
}
