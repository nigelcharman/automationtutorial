package assurity.web.presence;

import org.concordion.api.extension.Extension;
import org.concordion.ext.ScreenshotExtension;
import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.After;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import driver.core.DriverFactory;
import driver.core.SeleniumScreenshotTaker;
import driver.google.search.GoogleResultsPage;
import driver.google.search.GoogleSearchPage;

@RunWith(ConcordionRunner.class)
public class AssuritySearchTest {

    private WebDriver driver = new DriverFactory().getDriver();
    
    @Extension
    public ScreenshotExtension screenshotExtension = new ScreenshotExtension().setScreenshotTaker(new SeleniumScreenshotTaker(driver));

    public String resultFor(String searchProvider, String keywords) {
        GoogleSearchPage searchPage = new GoogleSearchPage(driver);
        GoogleResultsPage resultsPage = searchPage.searchFor(keywords);
        return resultsPage.getResult();
    }

    @After
    public void closeBrowser() {
        driver.close();
    }
}