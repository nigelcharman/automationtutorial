package nz.co.assurity.pages;

import java.util.ArrayList;
import java.util.List;

import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * A Thucydides Page Object corresponding to the Google Results Page.
 */
public class GoogleResultsPage extends PageObject {

    @FindBy(className = "s")
    private List<WebElement> resultsText;

    @FindBy(className = "l")
    private WebElement firstResultLink;

    @FindBy(className = "r")
    private WebElement resultLink;

    @FindBy(id = "foot")
    private WebElement footer;

    public GoogleResultsPage(WebDriver driver) {
        super(driver);
    }

    @WhenPageOpens
    public void waitForFooter() {
        element(footer).waitUntilVisible();
    }
    
    public String getTopResultTitle() {
        return firstResultLink.getText();
    }

    public List<String> getResults() {
        List<String> results = new ArrayList<String>(resultsText.size());
        for (WebElement result : resultsText) {
            results.add(result.getText());
        }
        return results;
    }

    public String getResult() {
        return resultLink.getText();
    }

}