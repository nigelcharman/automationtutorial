package nz.co.assurity.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * A Thucydides Page Object codrresponding to the Google Search Page.
 */
@DefaultUrl("http://www.google.com")
public class GoogleSearchPage extends PageObject {
	
	@FindBy(name = "q") 
	private WebElement queryBox;
	
	/**
	 * Opens the Google Search Page.
	 */
	public GoogleSearchPage(WebDriver driver) {
	    super(driver);
	}

	public void enter_keywords(String query) {
	    queryBox.clear();
	    queryBox.sendKeys(query);
	}

	public void start_search() {
        queryBox.submit();
	}
}