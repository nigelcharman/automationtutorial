package nz.co.assurity

import nz.co.assurity.steps.EndUserSteps
import nz.co.assurity.requirements.Application.Search.SearchByKeyword

using "thucydides"

thucydides.uses_default_base_url "http://www.google.com"
thucydides.uses_steps_from EndUserSteps
thucydides.tests_story SearchByKeyword

scenario "Looking up 'Assurity' should show the welcome page", {
    given "the user is on the Google home page", {
        end_user.is_the_home_page()
    }
    when "the end user looks up the definition of the word 'Assurity'", {
        end_user.looks_for "Assurity"
    }
    then "they should see 'Assurity - Ensuring Software Health - Welcome'", {
       end_user.should_see_the_top_result_of "Assurity - Ensuring Software Health - Welcome"
    }
}

scenario "Looking up agile testing should show training courses", {
    given "the user is on the Google home page", {
        end_user.is_the_home_page()
    }
    when "the end user looks up the definition of the word 'Assurity'", {
        end_user.looks_for "test agile new zealand"
    }
    then "they should see 'Assurity - Ensuring Software Health - Welcome'", {
       end_user.should_see_the_top_result_of "Agile Testing Training Courses in Auckland New Zealand"
    }
}

