package nz.co.assurity;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.runners.ThucydidesRunner;
import nz.co.assurity.requirements.Application;
import nz.co.assurity.steps.EndUserSteps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@Story(Application.Search.SearchByKeyword.class)
@RunWith(ThucydidesRunner.class)
public class AssuritySearchTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = "http://www.google.com")
    public Pages pages;

    @Steps
    public EndUserSteps endUser;

    @Test
    public void looking_up_assurity_should_show_the_welcome_page() {
        endUser.is_the_home_page();
		endUser.looks_for("Assurity");
        endUser.should_see_the_top_result_of("Assurity - Ensuring Software Health - Welcome");
    }

    @Test
    public void looking_up_agile_testing_should_show_training_courses() {
        endUser.is_the_home_page();
        endUser.looks_for("test agile new zealand");
        endUser.should_see_the_top_result_of("Agile Testing Training Courses in Auckland New Zealand");
    }
} 