package nz.co.assurity.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import nz.co.assurity.pages.GoogleResultsPage;
import nz.co.assurity.pages.GoogleSearchPage;

public class EndUserSteps extends ScenarioSteps {

    public EndUserSteps(Pages pages) {
        super(pages);
    }

    @Step
    public void enters(String keyword) {
        onGoogleSearchPage().enter_keywords(keyword);
    }

    @Step
    public void starts_search() {
        onGoogleSearchPage().start_search();
    }

    private GoogleSearchPage onGoogleSearchPage() {
        return getPages().currentPageAt(GoogleSearchPage.class);
    }

    private GoogleResultsPage GoogleResultsPage() {
        return getPages().currentPageAt(GoogleResultsPage.class);
    }

    @Step
    public void should_see_a_result_containing_words(String terms) {
        assertThat(GoogleResultsPage().getResults(), hasItem(containsString(terms)));
    }

    @Step
    public void should_see_the_top_result_of(String result) {
        assertThat(GoogleResultsPage().getResult(), is(result));
    }

    @Step
    public void is_the_home_page() {
        onGoogleSearchPage().open();
    }

    @Step
    public void looks_for(String term) {
        enters(term);
        starts_search();
    }
}
